var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Customers Web App' });
});

/* GET home page. */
router.get('/demo.html', function(req, res, next) {
  res.render('index', { title: 'Customers Web App' });
});

/* GET home page. */
router.get('/cust/demo.html', function(req, res, next) {
  res.render('index', { title: 'Customers Web App' });
});

module.exports = router;
