var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Employees Web App' });
});

/* GET home page. */
router.get('/demo.html', function(req, res, next) {
  res.render('index', { title: 'Employees Web App' });
});

/* GET home page. */
router.get('/empl/demo.html', function(req, res, next) {
  res.render('index', { title: 'Employees Web App' });
});

module.exports = router;
